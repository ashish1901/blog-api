from django.urls import path
from rest_framework.routers import SimpleRouter
from posts import views
# from posts.views import PostList, PostDetail, UserList, UserDetail

router = SimpleRouter()
router.register('users', views.UserViewSet, basename='users')
router.register('', views.PostViewSet, basename='posts')

urlpatterns = router.urls

# urlpatterns = [
#     # path('users/', UserList.as_view()),
#     # path('users/<int:pk>/', UserDetail.as_view()),
#     # path('<int:pk>/', PostDetail.as_view()),
#     # path('', PostList.as_view()),
# ]