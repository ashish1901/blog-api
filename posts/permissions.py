from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # if request is read-only, then permission granted
        if request.method in permissions.SAFE_METHODS:
            return True

        # if request is not read-only, then only author should be granted access
        return obj.author == request.user
