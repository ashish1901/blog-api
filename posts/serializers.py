from django.contrib.auth import get_user_model
from rest_framework import serializers
from posts.models import Post


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'title', 'author', 'body', 'created_at')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        # get_user_model() gets the current used User model, it can be built-in one, or the custom one
        model = get_user_model()
        fields = ('id', 'username')
